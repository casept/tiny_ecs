use tiny_ecs::{ECSError, Entities};

struct Test1 {}
struct Test2 {}
struct Test3 {}

fn set_up_entity_two_components() -> Result<Entities, ECSError> {
    let mut entities = Entities::new(Some(3), Some(3));
    let entity_1 = entities
        .new_entity()
        .with(Test1 {})?
        .with(Test2 {})?
        .finalise()?;
    assert_eq!(entity_1, 0);
    assert!(entities.entity_exists(0));
    assert!(entities.entity_contains::<Test1>(0));
    assert!(entities.entity_contains::<Test2>(0));
    Ok(entities)
}

#[test]
fn check_entity_contains() {
    let entities = set_up_entity_two_components().unwrap();
    assert!(!entities.entity_contains::<Test3>(0));
}

#[test]
fn with_is_err_after_finalise() {
    let mut entities = set_up_entity_two_components().unwrap();
    assert!(entities.with(Test3 {}).is_err());
}

#[test]
fn finalise_is_err_after_finalise() {
    let mut entities = set_up_entity_two_components().unwrap();
    assert!(entities.finalise().is_err());
}

#[test]
fn is_err_if_add_same_component() {
    let mut entities = set_up_entity_two_components().unwrap();
    assert!(entities.add_component(0, Test1 {}).is_err());
}

#[test]
#[should_panic]
fn panic_if_add_component_to_none_entity() {
    let mut entities = Entities::new(Some(3), Some(3));
    entities.add_component(0, Test1 {}).unwrap();
}

#[test]
fn components_ref_and_mut_diff_components_and_modify() {
    #[derive(Debug, PartialEq)]
    struct Test1 {
        x: u32,
    }

    #[derive(Debug, PartialEq)]
    struct Test2 {
        x: u32,
    }

    let mut entities = Entities::new(Some(3), Some(3));
    let entity_1 = entities
        .new_entity()
        .with(Test1 { x: 66 })
        .unwrap()
        .with(Test2 { x: 42 })
        .unwrap()
        .finalise()
        .unwrap();

    let components = entities.borrow::<Test1>().unwrap();
    let part = components.get(entity_1).unwrap();
    assert_eq!(part, &Test1 { x: 66 });
    assert!(entities.entity_contains::<Test1>(entity_1));

    {
        let mut components = entities.borrow_mut::<Test2>().unwrap();
        let part = components.get_mut(entity_1).unwrap();
        assert_eq!(part.x, 42);
        part.x = 666;
        assert_ne!(part.x, 42);
    }
    let mut _p = entities.borrow_mut::<Test2>().unwrap();
    assert!(entities.borrow_mut::<Test2>().is_err());
}

#[test]
fn components_iter_ref() {
    let mut entities = Entities::new(Some(3), Some(3));
    struct Test1 {
        x: usize,
    }

    let mut ids = Vec::new();
    // 1
    let id = entities
        .new_entity()
        .with(Test1 { x: 0 })
        .unwrap()
        .finalise()
        .unwrap();
    ids.push(id);
    // 2
    let id = entities
        .new_entity()
        .with(Test1 { x: 1 })
        .unwrap()
        .finalise()
        .unwrap();
    ids.push(id);
    // 3
    let id = entities
        .new_entity()
        .with(Test1 { x: 2 })
        .unwrap()
        .finalise()
        .unwrap();
    ids.push(id);

    let components = entities.borrow::<Test1>().unwrap();
    for (k, v) in components.iter() {
        assert!(v.x == k);
    }
}

#[test]
fn components_get_mut() {
    struct Test1 {
        x: usize,
    }

    let mut entities = Entities::new(Some(3), Some(3));
    let mut ids = Vec::new();
    // 1
    let id = entities
        .new_entity()
        .with(Test1 { x: 0 })
        .unwrap()
        .finalise()
        .unwrap();
    ids.push(id);
    // 2
    let id = entities
        .new_entity()
        .with(Test1 { x: 1 })
        .unwrap()
        .finalise()
        .unwrap();
    ids.push(id);
    // 3
    let id = entities
        .new_entity()
        .with(Test1 { x: 2 })
        .unwrap()
        .finalise()
        .unwrap();
    ids.push(id);

    let mut components = entities.borrow_mut::<Test1>().unwrap();
    for (k, v) in components.iter_mut() {
        v.x += 1;
        assert!(v.x > k);
    }
}

#[test]
fn remove_components_then_entity() {
    let mut entities = set_up_entity_two_components().unwrap();

    assert!(entities.rm_component::<Test1>(0).is_ok());

    assert!(!entities.entity_contains::<Test1>(0));

    // Removing all components erases the entity
    assert!(entities.rm_component::<Test2>(0).is_ok());

    assert!(!entities.entity_contains::<Test2>(0));

    assert!(!entities.entity_exists(0));
}

#[test]
fn remove_entity() {
    let mut entities = set_up_entity_two_components().unwrap();

    assert!(entities.rm_component::<Test1>(0).is_ok());

    assert!(!entities.entity_contains::<Test1>(0));

    // Removing all components erases the entity
    entities.rm_entity(0);

    assert!(!entities.entity_exists(0));
}
