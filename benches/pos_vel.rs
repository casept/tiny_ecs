use criterion::*;
use itertools::*;
use tiny_ecs::Entities;

/// Entities with velocity and position component.
pub const N_POS_PER_VEL: usize = 10;

/// Entities with position component only.
pub const N_POS: usize = 10000;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Position {
    pub x: f32,
    pub y: f32,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Velocity {
    pub dx: f32,
    pub dy: f32,
}

fn build() -> Entities {
    let mut entities = Entities::new(Some(500), Some(12));

    for i in 0..N_POS {
        entities.new_entity();
        entities.with(Position { x: 0.0, y: 0.0 }).unwrap();
        if i % N_POS_PER_VEL == 0 {
            entities.with(Velocity { dx: 0.0, dy: 0.0 }).unwrap();
        }
        // finished with this entity
        entities.finalise().unwrap();
    }
    entities
}

fn bench_build(c: &mut Criterion) {
    c.bench_function("Pos/Vel build", |b| {
        b.iter(|| build());
    });
}

fn bench_update_safe(b: &mut Bencher, _i: &u32) {
    let world = build();

    let vel = world.borrow::<Velocity>().unwrap();
    let mut pos = world.borrow_mut::<Position>().unwrap();

    b.iter(|| {
        for (id, vel) in vel.iter() {
            if let Some(pos) = pos.get_mut(id) {
                pos.x += vel.dx;
                pos.y += vel.dy;
            }
        }
    });
}

fn bench_update_unsafe(b: &mut Bencher, _i: &u32) {
    let world = build();

    let vel = unsafe { world.borrow_unchecked::<Velocity>().unwrap() };
    let pos = unsafe { world.borrow_mut_unchecked::<Position>().unwrap() };

    b.iter(|| {
        for (id, vel) in vel.iter() {
            if let Some(pos) = pos.get_mut(id) {
                pos.x += vel.dx;
                pos.y += vel.dy;
            }
        }
    });
}

fn bench(c: &mut Criterion) {
    let update_safe = Fun::new("safe", bench_update_safe);
    let update_unsafe = Fun::new("unsafe", bench_update_unsafe);
    let fun = vec![update_safe, update_unsafe];
    c.bench_functions("Pos/Vel update", fun, 10000);
}

criterion_group!(benches, bench_build, bench,);
criterion_main!(benches);
