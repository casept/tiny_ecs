use criterion::*;
use itertools::*;
use tiny_ecs::Entities;

#[derive(Copy, Clone, Debug, PartialEq)]
struct A(f32);

#[derive(Copy, Clone, Debug, PartialEq)]
struct B(f32);

#[derive(Copy, Clone, Debug, PartialEq)]
struct C(f32);

#[derive(Copy, Clone, Debug, PartialEq)]
struct D(f32);

#[derive(Copy, Clone, Debug, PartialEq)]
struct E(f32);

#[derive(Copy, Clone, Debug, PartialEq)]
struct F(f32);

#[derive(Copy, Clone, Debug, PartialEq)]
struct G(f32);

#[derive(Copy, Clone, Debug, PartialEq)]
struct Position(f32);

#[derive(Copy, Clone, Debug, PartialEq)]
struct Rotation(f32);

fn create_entities(entities: &mut Entities, num_components: usize, count: usize) {
    let groups = (0..count * num_components)
        .flat_map(|step| (0..7).map(move |i| ((i + i * step) % 7) as u16))
        .chunks(num_components);

    for group in groups.into_iter() {
        entities.new_entity();
        for index in group {
            match index {
                0 => entities.with(A(0.)),
                1 => entities.with(B(0.)),
                2 => entities.with(C(0.)),
                3 => entities.with(D(0.)),
                4 => entities.with(E(0.)),
                5 => entities.with(F(0.)),
                _ => entities.with(G(0.)),
            };
        }
    }
}

fn add_background_entities(entities: &mut Entities, count: usize) {
    create_entities(entities, 4, count);
}

fn setup(n: usize) -> Entities {
    let mut entities = Entities::new(Some(n), Some(12));

    for _ in 0..n {
        entities
            .new_entity()
            .with(Position(0.))
            .unwrap()
            .with(Rotation(0.))
            .unwrap()
            .finalise()
            .unwrap();
    }
    entities
}

fn bench_create_delete(c: &mut Criterion) {
    c.bench_function_over_inputs(
        "create-delete",
        |b, count| {
            let mut entities = setup(0);
            b.iter(|| {
                let entity_ids: Vec<usize> = (0..*count)
                    .map(|_| {
                        entities
                            .new_entity()
                            .with(Position(0.))
                            .unwrap()
                            .finalise()
                            .unwrap()
                    })
                    .collect();
                for e in entity_ids {
                    entities.rm_component::<Position>(e).unwrap();
                }
            })
        },
        (0..10).map(|i| i * 100),
    );
}

fn bench_iter_safe(b: &mut Bencher, i: &u32) {
    let mut entities = setup(*i as usize);
    add_background_entities(&mut entities, *i as usize);

    let mut rots = entities.borrow_mut::<Rotation>().unwrap();
    let pos = entities.borrow::<Position>().unwrap();

    b.iter(|| {
        for (id, pos) in pos.iter() {
            if let Some(rot) = rots.get_mut(id) {
                rot.0 = pos.0
            }
        }
    });
}

fn bench_iter_unsafe(b: &mut Bencher, i: &u32) {
    let mut entities = setup(*i as usize);
    add_background_entities(&mut entities, *i as usize);

    let rots = unsafe { entities.borrow_mut_unchecked::<Rotation>().unwrap() };
    let pos = unsafe { entities.borrow_unchecked::<Position>().unwrap() };

    b.iter(|| {
        for (id, pos) in pos.iter() {
            if let Some(rot) = rots.get_mut(id) {
                rot.0 = pos.0
            }
        }
    });
}

fn bench(c: &mut Criterion) {
    let iter_safe = Fun::new("iter_safe", bench_iter_safe);
    let iter_unsafe = Fun::new("iter_unsafe", bench_iter_unsafe);
    let fun = vec![iter_safe, iter_unsafe];
    c.bench_functions("Iteration and modify", fun, 10000);
}

criterion_group!(benches, bench_create_delete, bench,);
criterion_main!(benches);
