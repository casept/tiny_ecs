use std::error::Error;
use std::fmt;
use std::fmt::{Debug, Display};

pub enum ECSError {
    Borrow,
    BorrowMut,
    Downcast,
    DowncastMut,
    PtrRef,
    PtrMut,
    NoComponentMap,
    NoComponentForEntity,
    WithAfterFinalise,
    FinaliseNonEntity,
    BitMasksExhausted,
    AddDupeComponent,
}

impl Debug for ECSError {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(self.description(), f)
    }
}

impl Display for ECSError {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(self.description(), f)
    }
}

impl Error for ECSError {
    fn description(&self) -> &str {
        match self {
            ECSError::Borrow => "already borrowed",
            ECSError::BorrowMut => "already mutably borrowed",
            ECSError::Downcast => "could not downcast to ref",
            ECSError::DowncastMut => "could not downcast to mut ref",
            ECSError::PtrRef => "failed to convert pointer to ref",
            ECSError::PtrMut => "failed to convert pointer to mut",
            ECSError::NoComponentMap => "no component map for type",
            ECSError::NoComponentForEntity => "part map does not contain part for entity",
            ECSError::WithAfterFinalise => "use of .with() after .finalise()",
            ECSError::FinaliseNonEntity => "can not finalise() without new_entity()",
            ECSError::BitMasksExhausted => "bitmasks exhausted, can't add new component type",
            ECSError::AddDupeComponent => "attempted to add duplicate component to entity",
        }
    }
}
