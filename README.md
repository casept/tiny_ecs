[![Build Status](https://drone.systemscoder.nz/api/badges/luke/tiny-ecs/status.svg)](https://drone.systemscoder.nz/luke/tiny-ecs) [[**Documentation**](https://docs.rs/tiny_ecs)] [[**Changelog**](CHANGELOG.md)] [[**License: MPL-2.0**](LICENSE)]

# Tiny ECS

This ECS intends to be simple, fast, and easy to use. As such it provides
management for entities and components, but not systems - this part is up
to you.

You will need to create external systems; these can be a function, a loop,
or anything else you can think of to run - functionally you'll be doing
something with multiple `VecMap<T>`, which are what contain components.
One `VecMap` per component type.

Internally this ECS is the use of `bitmasks`. Each entity ID is in
practice an internal index number in to an array which contains bitmasks.
The bitmasks themselves keep track of what components the entity has.

**Note:** borrows of `ComponentMap` are checked at runtime.

## Benchmarks

Based on the [ecs_bench](https://github.com/lschmierer/ecs_bench) project.

![](misc/bench.png)

```
Pos/Vel build                   time:   [217.56 us 218.52 us 219.65 us]
Found 9 outliers among 100 measurements (9.00%)
  9 (9.00%) high mild
```

### Safe runtime borrow checking vs unchecked borrow.

```
Pos/Vel update/safe             time:   [5.9081 us 5.9175 us 5.9280 us]
Found 6 outliers among 100 measurements (6.00%)
  3 (3.00%) high mild
  3 (3.00%) high severe

Pos/Vel update/unsafe           time:   [5.9153 us 5.9242 us 5.9349 us]
Found 9 outliers among 100 measurements (9.00%)
  1 (1.00%) high mild
  8 (8.00%) high severe
```

### Creation and Adding Entities

```rust
// Create new container
let entities = Entities::new();

// The builder pattern is powerful
let ent = world .entities
                .new_entity()
                .with(ParticleID { id })?
                .with(Sprite::new(sprite_id, rec))?
                .with(InputTypes::Player(binds))?
                .with(Player::new(5.0, 15.0))?
                .finalise()?;
```

### Systems

```rust
// Example system
fn example_system(active_ents: &[usize], entities: &mut Entities) -> Result<(), ECSError> {
    // You can mutably borrow multiple component maps at once
    let mut v1_components = entities
        .borrow_mut::<Vector1>()?;

    let mut v2_components = entities
        .borrow_mut::<Vector2>()?;

    // But not have a mutable borrow and immutable borrow to the same map
    // Fails at runtime!
    // let v2_components = entities
    //     .borrow::<Vector2>()
    //     .expect("Can not borrow immutable when already borrowed mutably");
    for id in active_ents {
        if entities.entity_contains::<Vector1>(*id) &&
           entities.entity_contains::<Vector2>(*id) {
            let v1_part = v1_components.get_mut(*id)?;
            let v2_part = v2_components.get_mut(*id)?;
            v1_part.x = 42;
            assert_ne!(v1_part.x, 43);
            assert_eq!(v1_part.x, 42);
        }
    }
    Ok(())
}
```

Please see docs for further examples and help.
